# braintree_gateway
## Overview
This is a minimal demo implementation of the Braintree v.zero DropIn client 
[https://developers.braintreepayments.com/javascript+node/start/overview](Get Started with Braintree).

The demo is implemented two ways for illustrative purposes. The first way uses explicit APIs `./app/payments/client_token.js` and `./app/payments/payments_methods.js` on the Backendless server that communicate with the Braintree gateway.  The second way uses an API `./app/payments/gateway.js` that makes most of the requests supported by the Braintree gateway accessible to the client.

The demo also includes a version of the API `./app/payments/gateway_auth.js` that uses the Backendless User service to control access to the Braintree gateway.  Applications must use the Backendless User service to login and get a `user-token`.  The `user-token` must be presented to the Braintree gateway API `./app/payments/gateway_auth.js`, which then verifies it with the Backendless User service before proceeding with the request to the Braintree gateway.

Finally, the demo also includes a cron job API `./app/cron/update_status.js` and `./app/cron/update_status_auth.js`, that is used to update a Backendless application that tracks Braintree operation.

## Tech Overview
This demo integrates an Express router `./server` sub-package with an application `./app` sub-package.  The integration is done in the `./app/api.js` module.  The `server.js` package is the top-level script for the app.

One can supply a self-signed TLS certificate for the server to test HTTPS operation.  The TLS files `./cert/key.pem`, `./cert/csr.pem`, and `./cert/certificate.pem` are not stored in the repo.  They must be created on the server itself.

Backendless and Braintree configuration info for the application can be supplied via environment variables or in two configuration files `./config/backendless_user.js` and `./config/braintree.js`. This folder and the contents are not stored in the GitHub repo and must be manually created on the server instance.  The environment variables or config files must be created before the server is started.

## Server Core Setup
This package includes the Express server code and other packages for a simple server framework that supports HTTP/HTTPS communications and which can be directed to listen on any port.  The server core is found in the `./server` folder.

### Certificate for HTTPS SSL/TLS
The top-level module `server.js` looks for the key and certificate information in two files in a folder in the project that is not stored in the GitHub repo:
```
./cert/key.pem
./cert/certificate.pem
```
More information about creating these credentials can be found here:
```
https://nodejs.org/api/https.html
https://nodejs.org/api/tls.html
```

The locally-signed certificate may be created as:
```
$ mkdir cert
$ cd cert
$ openssl genrsa -out key.pem 2048
$ openssl req -new -sha256 -key key.pem -out csr.pem
$ openssl x509 -req -in csr.pem -signkey key.pem -out certificate.pem
```
In the step to create `csr.pem` answer the question (the specific question depends on the version of `openssl`):
```
Common Name (e.g. server FQDN or YOUR name) []:localhost
Common Name (eg, your name or your server's hostname) []:"public-dns"
```
Note: It may be necessary to regenerate these files after Dec. 31 of each year, or one year after creation, for the next year.

### Server Configuration info
The server configuration info may be supplied as a shell script that sets environment variables, *e.g.* `./config/appserver_local_http.sh`:
```
#!/bin/sh
#
export PROTOCOL=https
export PORT=3000
```
This script must be run before the server is started
```
$ source config/appserver_local_https.sh
```

Configuration may also be supplied as a Javascript module `./config/appserver.js`
```
/**
 * Localhost https configuration returned as a JS object
 */
;(function() {
	module.exports = {
		protocol: "https",
		port: "3000"
	}
}).call(this);
```

On startup, `server.js` will first look if `config/appserver.js` exists and use the contents.  If this file doesn't exist, `server.js` will look if the environment variables have been defined and use them. If neither exists, the defaults will be used:
```
configAppPrm.port = 3000;
configAppPrm.protocol = "https";
```

## Application Setup
The application includes REST API handlers for communicating with the Braintree gateway and for a cron job which manages the Backendless Data service tables.   The application handlers are found in the `./app` folder.  The server and application handlers are linked in the `./api.js` module.

### Backendless User Service Configuration
The configuration info for the Backendless User service may be supplied as a shell script that sets environment variables, *e.g.* `./config/backendless_user.sh`:
```
#!/bin/sh
#
export BACKENDLESS_APP_ID=*Application ID*
export BACKENDLESS_JAVASCRIPT_SECRET_KEY=*Javascript Secret Key*
export BACKENDLESS_REST_SECRET_KEY=*REST Secret Key*
export BACKENDLESS_APP_VERSION=v1
```
This script must be run before the server is started
```
$ source config/backendless_user.sh
```

Configuration may also be supplied as a Javascript module `config/backendless_user.js`
```
;(function() {
	module.exports = {
	appId: *Application ID*,
	javascriptSecretKey: *Javascript Secret Key*,
	restSecretKey: *REST Secret Key*,
	appVersion: "v1"
	}
}).call(this);
```
The Backendless credentials can be found in Backendless Developers console under **Manage** -> **App Settings** selection for the app.

On startup, `./api.js` will first look if `./config/backendless_user.js` exists and use the contents.  If this file doesn't exist, `./api.js` will look if the environment variables have been defined and use them.

### Braintree Service Configuration
The configuration info for the Braintree service may be supplied as a shell script that sets environment variables, *e.g.* `./config/braintree.sh`:
```
#!/bin/sh
#
export BRAINTREE_MERCHANT_ID=*Merchant ID*
export BRAINTREE_PUBLIC_KEY=*Public Key*
export BRAINTREE_PRIVATE_KEY=*Private Key*
```
This script must be run before the server is started
```
$ source config/braintree.sh
```

Configuration may also be supplied as a Javascript module `./config/braintree.js`
```
;(function() {
	module.exports = {
		merchantId: *Merchant ID*,
		publicKey: *Public Key*,
		privateKey: *Private Key*
	}
}).call(this);
```
The Braintree credentials can be found in the [https://sandbox.braintreegateway.com/login](Braintree Dashboard) under "Authorization" item of the **Account** -> **My user** selection.

On startup, `./api.js` will first look if `./config/braintree.js` exists and use the contents.  If this file doesn't exist, `./api.js` will look if the environment variables have been defined and use them.

## CRON job setup
The Backendless system managing Braintree transactions in the Data service should be configured with a special user that has access to the Braintree Data service tables. The CRON job can then login as this special user to update the transaction status.  The CRON job also uses the Application Backendless and Braintree configuration info described previously, it does NOT use the Server configuration since it directly access Backendless and Braintree.

### CRON job user configuration
The user configuration info for the CRON job may be supplied as a shell script that sets environment variables, *e.g.* `./config/appcron.sh`:
```
#!/bin/sh
#
export APPCRON_USER=*CRON User name*
export APPCRON_PASSWORD=*CRON User password*
```
This script must be run before the server is started
```
$ source config/appcron.sh
```

Configuration may also be supplied as a Javascript module `./config/appcron.js`
```
;(function() {
	module.exports = {
		user: *CRON User name*,
		password: *CRON User password*
	}
}).call(this);
```
The Backendless system managing Braintree transactions in the Data service should 

On startup, `./cron.js` will first look if `./config/appcron.js` exists and use the contents.  If this file doesn't exist, `./cron.js` will look if the environment variables have been defined and use them.


## Deployment on AWS EC2
The application can be installed on an AWS EC2 instance in a straightforward process.

### Installation 
####1. Clone the repo
In the AWS EC2 instance, clone the application
```
$ cd *app_directory*
$ git clone https://bitbucket.org/iflipddevteam/braintree_gateway.git
```

Modifications to the app pushed to the repo can be added to the cloned version with
```
$ cd *app_directory*
$ git pull
```

####2. Build the app
Pull in the necessary dependencies using `npm`:
```
$ npm install
```

####3. Install configuration info
From the AWS EC2 command line, create the `./config` directory:
```
$ cd *app_directory*/braintree_gateway
$ mkdir config
```
The preferred configuration files then can be uploaded from a local machine using `scp`:
```
$ scp -i *key-pair*.pem appserver.js *ec2-user@public-dns*:/*app_directory*/braintree_gateway/config/appserver.js

$ scp -i *key-pair*.pem backendless_user.js *ec2-user@public-dns*:/*app_directory*/braintree_gateway/config/backendless_user.js

$ scp -i *key-pair*.pem backendless.js *ec2-user@public-dns*:/*app_directory*/braintree_gateway/config/backendless.js
```

####4. Install SSL certificate
From the AWS EC2 command line, create the `cert` directory and upload an SSL certificate from a local machine
```
$ cd *app_directory*/braintree_gateway
$ mkdir cert
$ scp -i *key-pair*.pem certificate.pem *ec2-user@public-dns*:/*app_directory*/braintree_gateway/cert/certificate.pem
```

Alternatively, a self-signed SSL certificate may be created in the `cert/` folder using the procedure described above.

### Configure AWS EC2 ports
The AWS EC2 Management Console and EC2 instance command line may be used to configure the IP ports on the instance.  NOTE: The instructions for working at the EC2 instance command line here are for working from a user account with `sudo` privileges.

####1. Open the ports in the AWS cloud
In the ECS Management Console, under the **Instances** section select the *Instances* page, select the instance in the top panel, then in the bottom panel note the desired entry under "Security groups" (e.g., "launch-wizard-4")

Under the **Network & Security** section, select the *Security Groups* page, then select the security group for the instance in the top panel.

In the bottom panel, select the "Inbound" tab, click the "Edit" button, and add rules:
```
Type      Protocol    Port Range    Source
----      --------    ----------    ------
HTTP      TCP         80            Anywhere 0.0.0.0/0
HTTPS     TCP         443           Anywhere 0.0.0.0/0
```
then hit the "Save" button to add the rules.

####2. Enable IP port forwarding in the EC2 instance
From the coomand line in the EC2 instance, check if IP Forwarding is enabled
```
$ cat /proc/sys/net/ipv4/ip_forward
```
(0 = not enabled, 1 = enabled)

To enable the IP Forwarding, edit the file
```
$ sudo vi /etc/sysctl.conf
```
uncomment the line, or set the value
```
# Controls IP packet forwarding
net.ipv4.ip_forward = 1
```
and enable the change
```
$ sudo sysctl -p /etc/sysctl.conf
```

####3. Configure port forwarding
From the EC2 instance command line:
```
$ sudo iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080
$ sudo iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 443 -j REDIRECT --to-port 8443
```

####4. Open the EC2 instance firewall
From the EC2 instance command line:
```
$ sudo iptables -A INPUT -p tcp -m tcp --sport 80 -j ACCEPT
$ sudo iptables -A OUTPUT -p tcp -m tcp --dport 80 -j ACCEPT

$ sudo iptables -A INPUT -p tcp -m tcp --sport 443 -j ACCEPT
$ sudo iptables -A OUTPUT -p tcp -m tcp --dport 443 -j ACCEPT
```

####5. Persist the EC2 instance iptables configuration
(To be verified)
From the EC2 instance command line:
```
$ sudo service iptables save
```
This should write the configuration info to `/etc/sysconfig/iptables`.

####6. If necessary start/stop iptables service
Start the service with:
```
$ sudo service iptables start
```
Stop the service with:
```
$ sudo service iptables stop
```
Check the iptables configuration with
```
$ sudo service iptables status
```

### Manually starting the server
The Express server can be manually started with the command
```
$ cd *app_directory*/braintree_gateway
$ node server.js
```

### Server setup for start on AWC EC2 instance boot/reload
Setting up the server so that it automatically starts on instance boot/reload requires additions to `/etc/rc.d/rc.local`.  A modified version of this file is found in `./bin/rc.local`.

####1. Install the `forever` script
The `forever` script allows one to run a script as a daemon, restarting it if it fails.  From the EC2 instance command line it is installed as:
```
$ sudo /usr/local/bin/npm install forever -g
```
NOTE: `forever` appears to not be well maintained so it may be necessary to fork and manually maintain this package.

####2. Test the `forever` script
Test the "forever" script if desired from the EC2 instance command line:
```
$ forever start server.js
$ forever stop server.js
```

####3. Install in `crontab`
It seems one way to handle persistent tasks on EC2 instances is to install a `cron` "reboot" task. From the EC2 instance command line
```
$ sudo crontab -u *username* -e
```
Then in the editor screen:
```
@reboot /home/*username*/*app_directory*/braintree_gateway/bin/server.sh
```

Note: This version of the edit command makes the crontab for `/var/spool/cron/*username*` owned by `root`. This version
```
$ crontab -e
```
makes a crontab owned by `*username*`.


####4. Install `rc.local`
TBD if this is needed or preferable.  A draft `./bin/rc.local/` script is included in the project.


## API Documentation
### Test APIs
Several test REST APIs are included in the application.

The REST API URLs that returns "Hello World":
```
https://*host_domain*/monitor/hello.js
```

The REST API URLS for a pinging the server:
```
https://*host_domain*/monitor/ping.js
https://*host_domain*/monitor/ping_auth.js
```

The cURL commands for accessing these functions in the REST API are:
```
curl -i -k -X GET https://*host_domain*/hello.js

curl -i -k -H "application-type:REST" \
-H "Content-Type:application/json" \
-d '{"monitor":"ping.js"}' \
-X POST "https://*host_domain*/monitor/ping.js"

curl -i -k -H "application-type:REST" \
-X GET "https://*host_domain*/monitor/ping.js?monitor=ping.js"

curl -i -k -H "application-type:REST" \
-H "Content-Type:application/json" \
-d '{"user-token":"<user-token>", "monitor":"ping_auth.js"}' \
-X POST "https://*host_domain*/monitor/ping_auth.js"

curl -i -k -H "application-type:REST" \
-X GET "https://*host_domain*/monitor/ping_auth.js?user-token=<user-token>&monitor=ping_auth.js"
```

For an installation on AWS EC2 instance, the *host_domain* is the *public_dns* of the AWS EC2 instance.

### Braintree Gateway REST API
The Braintree Gateway API makes Braintree gateway functions available to client apps.  This functionality is implemented as API request handlers in the `./app/payments` folder. The application makes much of the Braintree gateway available to a client through a REST API on Backendless in two forms.  The first does not require the user provide any Backendless credentials. The second requires the Backendless REST API Secret Key and that the user be logged into the Backendless User service.  The Braintree gateway routines are described in the Braintree docs
[https://developers.braintreepayments.com/javascript+node/reference/overview](Braintree Reference Overview)

The REST API URLs for the Braintree gateway are:
```
https://*host_domain*/payments/gateway.js
https://*host_domain*/payments/gateway2.js
https://*host_domain*/payments/gateway_auth.js
```
The `gateway.js` and `gateway2.js` URLs perform the same functions.  As shown below, the only difference between them is in the structure of the JSON object provided in the POST body.

The cURL commands for accessing the REST API are:
```
curl -i -k -H "application-type:REST" \
-H "Content-Type: application/json" \
-d '{ "request":APIrequest, "method":APImethod, ... }' \
-X POST https://*host_domain*/payments/gateway.js

curl -i -k -H "application-type:REST" \
-H "Content-Type: application/json" \
-d '{ "request":"<APIrequest>", "method":"<APImethod>", "params":"[{...}]"}' \
-X POST https://*host_domain*/payments/gateway2.js

curl -i -k -H "application-type:REST" \
-H "Content-Type: application/json" \
-d '{ "user-token":"<user-token>", "request":"<APIrequest>", "method":"<APImethod>", "params":"[{...}]" }' \
-X POST https://*host_domain*/payments/gateway_auth.js
```
where the `...` are the rest of the parameters for the specific gateway request. In the `gateway_auth.js` example, `<user-token>` is the Backendless token for a logged-in user.

The API also support GET requests with query parameters, e.g.
```
curl -i -k -H "application-type:REST" \
 -X GET https://*host_domain*/payments/gateway2.js?request=<APIRequest>&method=<APIMethod>&params=%5B%7B...%7D%5D"

curl -i -k -H "application-type:REST" \
-d '{ "request":APIrequest, "method":APImethod, ... }' \
-X GET https://*host_domain*/payments/gateway_auth.js?user-token=<user-token>&request=<APIRequest>&method=<APIMethod>&params=%5B%7B...%7D%5D"
```
(The GET processing has not been fully tested.)


### CRON Job REST API
The URLs for the CRON JOB REST API on Backendless are:
```
https://*host_domain*/payments/update_status.js
https://*host_domain*/payments/update_status2.js
https://*host_domain*/payments/update_status_auth.js
```
The `update_status.js` and `update_status2.js` URLs perform the same functions.  As shown below, the only difference between them is in the structure of the JSON object provided in the POST body.

The cURL commands for accessing the REST API have the general form:
```
curl -i -k -H "application-type:REST" \
-H "Content-Type:application/json" \
-d '{}' \
-X POST https://*host_domain*/cron/update_status.js

curl -i -k -H "application-type:REST" \
-H "user-token:<user-token>" \
-H "Content-Type:application/json" \
-d '{}' \
-X POST https://*host_domain*/cron/update_status2.js

curl -i -k -H "application-type:REST" \
-H "Content-Type:application/json" \
-d '{"user-token":"<user-token>"}' \
-X POST https://*host_domain*/cron/update_status_auth.js

curl -i -k -H "application-type:REST" \
-H "Content-Type:application/json" \
-d '{"user-token":"<user-token>", "pageSize":<page-size>, "offset":<offset>, "all":false }' \
-X POST https://*host_domain*/cron/update_status_auth.js
```
where `<page-size>` and `<offset>` are non-negative integer values.

## Braintree Gateway technical details

### Overview
The Braintree Gateway server consists of five pieces:

1. The `./bin/server.sh` shell script which invokes the Node.js javascript Braintree gateway server job.
2. The `./server.js` Node.js startup routine for the Braintree gateway server.
3. The `./api.js` Node.js module that links the Express `./server` to the Braintree gateway `./app`.
4. The `./server` Express server component of the Braintree Gateway server.
5. The `./app/payments` Braintree gateway server core routines.
6. The `./app/monitor`  Braintree gateway monitor routines.
 

### Braintree gateway startup shell script
The Braintree gateway startup shell script runs the `forever` command that in turn starts the `./server.js` routine. That is, 
the command:
```
$ ./bin/server.sh
```
in essence just runs
```
$ forever node /path/to/server.js
```

### Braintree gateway server routine
(`./server.js` TBD)

### Express server, app integration routine
(`./api.js` TBD)

### Express server component
(`./server` TBD)

### Braintree gateway core
(`./app/payments` TBD)

### Braintree gateway monitor
(`./app/monitor` TBD)


## CRON job technical details
The cron job to reconcile the status of transaction info in the Backendless Data service and on Braintree are implemented as API request handlers in the `./app/cron/` folder similar to the Braintree `./app/payments` API.  This API uses the Braintree Search function, which has a different form than most (all?) of the rest of the Braintree API Requests:
[https://developers.braintreepayments.com/javascript+node/reference/request/transaction/search](https://developers.braintreepayments.com/javascript+node/reference/request/transaction/search)

It is recommended that a special user be registered with the Backendless App to run the cron job to increase security.

### Overview
The CRON job consists of three pieces:

1. The `./bin/cron.sh` shell script which invokes the Node.js javascript CRON job
2. The `./cron.js` Node.js wrapper for the CRON job core.
3. The `./app/cron` CRON job core routines.

### CRON shell script
The CRON shell script just runs the `./cron.js` routine. That is, the command:
```
$ ./bin/cron.sh
```
in essence just runs
```
$ node /path/to/cron.js
```

### CRON wrapper processing overview
The `./cron.js` wrapper job consists of a cascade of three routines:

1. `login(options, callback)` - Uses the Backendless User management instance REST API to login as the CRON job and gets the `user-token` required by the CRON core.
2. `runupdate(options, callback)` - Calls the CRON core processing routine directly (not via REST API) with the `user-token` to update pending Braintree transactions that have been settled.
3. `logout(options, callback)` - Uses the `user-token` and the Backendless User management instance REST API to logout the CRON job.

### Internal routines
The code of the three modules is organized so that each has three entry points.  The `.fn` entry point is for direct calls, the `.hfn` entry point is a Node.js HTTP/HTTPS handler, and the `.run` entry point is a Backendless app handler.
```
// CRON core
var update_status = require('./app/cron/update_status.js').fn
var update_status = require('./app/cron/update_status.js').config(configBkUser, configBt).fn
update_status( callback );

var update_status = require('./app/cron/update_status.js').hfn
var update_status = require('./app/cron/update_status.js').config(configBkUser, configBt).hfn
update_status( req, res )

var update_status = require('./app/cron/update_status.js').run
var update_status = require('./app/cron/update_status.js').config(configBkUser, configBt).run
update_status( req, res )

// CRON core v2
var update_status2 = require('./app/cron/update_status2.js').fn
var update_status2 = require('./app/cron/update_status2.js').config(configBkUser, configBt).fn
update_status2( reqBody, callback );

var update_status2 = require('./app/cron/update_status2.js').hfn
var update_status2 = require('./app/cron/update_status2.js').config(configBkUser, configBt).hfn
update_status2( req, res )

var update_status2 = require('./app/cron/update_status2.js').run
var update_status2 = require('./app/cron/update_status2.js').config(configBkUser, configBt).run
update_status( req, res )

// user-token protected wrapper to regulate access to CRON core
var update_status_auth = require('./app/cron/update_status_auth.js').fn
var update_status_auth = require('./app/cron/update_status_auth.js').config(configBkUser, configBt).fn
update_status_auth( );  (NOTE: null routine not used right now)

var update_status_auth = require('./app/cron/update_status_auth.js').hfn
var update_status_auth = require('./app/cron/update_status_auth.js').config(configBkUser, configBt).hfn
update_status_auth( req, res )

var update_status_auth = require('./app/cron/update_status_auth.js').run
var update_status_auth = require('./app/cron/update_status_auth.js').config(configBkUser, configBt).run
update_status_auth( req, res )
```
Here `configBkUser` is an object that has the configuration info for the Backendless instance hosting User login management and `configBt` is an object with the Braintree gateway configuration.

To call the CRON job directly from another Node.js script, one uses the CRON core v2 routine. The calling routine must supply a valid Backendless `user-token` to allows the CRON job to access the Backendless Data service where the Braintree transactions are maintained:
```
var btA = path.resolve(__dirname, './app')  (optional prefix to path)
var update_status2_request = require(path.resolve(btA, './cron/update_status2.js')).config(configBkUser, configBt).fn;
update_status2_request( { 'user-token':*user-token* }, function(err, response) {...} );
```


### CRON core processing overview (historical design background)
The initial anticipated operation of the cron API was:

1.  Search the User table for set of records that have a PaymentGateway table child record with status "SUBMITTED FOR SETTLEMENT"(requires automatic Group By operation) and return objects to depth 2. If any records are returned, for each User record do Steps 2-4:
2.  Extract an array of structures `[{customer_id:  customer_id, transaction_ids: [ ]}, … ]` from the returned records
or just walk them as is. For each structure in the array, or equivalent data in the array of records, do Steps 3-4:
3.  Search Braintree for records with that `customer_id` and the `transaction_id` values with status "SETTLED".
4.  Update the "status" of PaymentGateway records from returned list of transactions.
5.  Repeat Step 1 if the last query returned a full set of records.

The currently proposed operation of the cron API is:

1.  Search the PaymentGateway table for a set of records that have the status "SUBMITTED FOR SETTLEMENT" and return objects to depth 1.  If any `PaymentGateway` records are returned, do Steps 2-5:
2.  Build an structure of objects (because Step 1 will use paging, this structure will have a limited number of key-value members):
```
{ payment_method.customer_id: [ transaction.payment_service_transaction_id, ... ], ... }
```
For each member of the structure do Steps 3-5:
3.  Search Braintree for records with the instance's `customer_id` with status "SETTLED".
4.  Prune the argument array of `transaction.payment_service_transaction_id` values to only those values in the list of transactions returned by Braintree.
5.  Update the `status` of `PaymentGateway` records from pruned list of transactions.
6.  Repeat Step 1 if the last query returned a full set of records.


### CRON core processing overview
The CRON core job in `./app/cron/update_status2.js` is organized into three cascaded routines:

1. `querySubmittedForSettlement(pageSize, offset, all, callback)` - Queries the Backendless `PaymentGateway` table for transactions whose status is `submitted_for_settlement`.
2. `queryBraintree(data, callback)` - Requests the transactions from the Braintree gateway that are `settled`, then saves only those transactions that are in the list from 1). That is, transactions whose status changed from `submitted_for_settlement` to `settled`.
3. `queryUpdateStatus(params, transactions, callback)` - Updates the Backendless `PaymentGateway` table status of transactions in 2).

`querySubmittedForSettlement(pageSize, offset, all, callback)` queries the Backendless `PaymentGateway` table using the Backendless REST API.  The routine requests `pageSize` transactions in the `submitted_for_settlement` state starting at `offset`.  (PROBLEM: Does this behave like a cursor so that any records in a request that are modified are not reconsidered on the next request, or as a de novo query each time?)   The retrieved records are first reorganized into a dictionary object by `customer_id`
```
var dataDict = {
    customer_id1: {
        "customerId": customer_id1,
        "transactionIds": [ ... ]
    },
      ...
}

```
The dictionary is then converted into an array suitable for the next stage
```
var dataArr = [
    {
        "customerId": customer_id1,
        "transactionIds": [ ... ]
    },
      ...
]
```
The `all` parameter indicates whether the entire table (`true`) or just a single page (`false`) should be processed.

`queryBraintree(data, callback)` queries the Braintree gateway using the Braintree Javascript SDK for `settled` transactions among those in the array of `submitted_for_settlement` transactions passed into it.   The query is done on a per-customer basis, one `dataArr` member per query.  Also, the Braintree API is a streaming API, so that this stage has to process the data for each customer in a streaming fashion.

Finally, `queryUpdateStatus(params, transactions, callback)` updates the Backendless database using the Backendless REST API. The update is done using the results from the previous phase, one customer at a time.

