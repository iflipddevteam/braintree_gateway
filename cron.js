/**
 * Module to run the cron job
 * 
 * Add to crontab with:
 * $ (crontab -l 2>/dev/null; echo "0 3 * * * node -with cron.js") | crontab -
 * 
 * Delete from crontab with:
 * $ crontab -r -u ec2-user
 */

;(function() {
	var request = require('request')
	  , fs = require('fs')
	  , path = require('path')
	  , stringify = require('json-stringify-safe')
	  , clone = require('clone');
	
	// Load Backendless configuration
	try {
		fs.accessSync(path.resolve(__dirname, './config/backendless_user.js'), fs.R_OK);
		var configBkUser = require(path.resolve(__dirname, './config/backendless_user.js'));
	}
	catch (ex) {
		var configBkUser = {};
		if(process.env.BACKENDLESS_APP_ID)
			configBkUser.appId = process.env.BACKENDLESS_APP_ID;
		if(process.env.BACKENDLESS_JAVASCRIPT_SECRET_KEY)
			configBkUser.javascriptSecretKey = process.env.BACKENDLESS_JAVASCRIPT_SECRET_KEY;
		if(process.env.BACKENDLESS_REST_SECRET_KEY)
			configBkUser.restSecretKey = process.env.BACKENDLESS_REST_SECRET_KEY;
		if(process.env.BACKENDLESS_APP_VERSION)
			configBkUser.appVersion = process.env.BACKENDLESS_APP_VERSION;
	};
	
	// Load Braintree configuration
	try {
		fs.accessSync(path.resolve(__dirname, './config/braintree.js'), fs.R_OK);
		var configBt = require(path.resolve(__dirname, './config/braintree.js'));
	}
	catch (ex) {
		var configBt = {};
		if(process.env.BRAINTREE_MERCHANT_ID)
			configBt.merchantId = process.env.BRAINTREE_MERCHANT_ID;
		if(process.env.BRAINTREE_PUBLIC_KEY)
			configBt.publicKey = process.env.BRAINTREE_PUBLIC_KEY;
		if(process.env.BRAINTREE_PRIVATE_KEY)
			configBt.privateKey = process.env.BRAINTREE_PRIVATE_KEY;
	};
	
	// Load cron user
	try {
		fs.accessSync(path.resolve(__dirname, './config/appcron.js'), fs.R_OK);
		var configAc = require(path.resolve(__dirname, './config/appcron.js'));
	}
	catch (ex) {
		var configAc = {};
		if(process.env.APPCRON_USER)
			configAc.user = process.env.APPCRON_USER;
		if(process.env.APPCRON_PASSWORD)
			configAc.password = process.env.APPCRON_PASSWORD;
	};
	
	// Get the core functionality of the cron job
	var btA = path.resolve(__dirname, './app')
	var update_status2_request = require(path.resolve(btA, './cron/update_status2.js')).config(configBkUser, configBt);
    
	// Log out step
	var logout = function(options, callback) {

		var asyncProcessing = function(callback) {

			var url = 'https://api.backendless.com/' + configBkUser.appVersion + '/users/logout';
		
			var requestOptions = {
				url: url,
				headers: {
					'application-id': configBkUser.appId,
					'secret-key': configBkUser.restSecretKey,
					'application-type': 'REST',
					'user-token': options['user-token']
				},
				json: true			
			};
		
			var requestCallback = function(err, response, body) {
				if(!response)
					response = {};
				response.cronStep = 'logout';
				response.cronBody = body;

				if(err) {
					callback(err, response);
					return;
				};
			
				callback(null, response);				
			};
		
			request.get(requestOptions, requestCallback); 
		};
	
		asyncProcessing(function(err, response) {
			callback( err, response );
		});		
	};
	
	// Run update step
	var runupdate = function(options, callback) {

		var asyncProcessing = function(callback) {

			var requestOptions = {
				all: true,
				"user-token": options['user-token']
			};
		
			function requestCallback(err, response) {
				if(!response)
					response = {};
				response.cronStep = 'runupdate';

				if(err) {
					callback(err, response);
					return;
				};
			
				logout(options, callback);
			};
		
			update_status2_request.fn(requestOptions, requestCallback);		
		};
	
		asyncProcessing(function(err, response) {
			callback( err, response );
		});		
	};
	
	// Log in step
	var login = function(options, callback) {

		var asyncProcessing = function(callback) {

			var url = 'https://api.backendless.com/' + configBkUser.appVersion + '/users/login';
		
			var requestOptions = {
				url: url,
				headers: {
					'application-id': configBkUser.appId,
					'secret-key': configBkUser.restSecretKey,
					'application-type': 'REST'
				},
				json: true,
				body: {login: configAc.user, password: configAc.password }			
			};
		
			var requestCallback = function(err, response, body){
				if(!response)
					response = {};
				response.cronStep = 'login';
				response.cronBody = body;

				if(err) {
					callback(err, response);
					return;
				};
			
				var callbackOptions = {
					"user-token": response.body['user-token']	
				}
			
				runupdate(callbackOptions, callback);
			};

			request.post(requestOptions, requestCallback);
		}
		
		asyncProcessing(function(err, response) {
			callback( err, response );
		});		
	};
	
	login( null, function(err, response) {} );
	
}).call(this);

