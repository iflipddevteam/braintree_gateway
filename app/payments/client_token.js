/**
 * Example Braintree API to get a client token
 */
;(function() {
	var braintree = require('braintree')
	  , path = require('path');
	
	var config;
	var gateway;
	
	var client_token = function( req, res ) {
		
		res.set('Access-Control-Allow-Origin', '*');
		
		gateway.clientToken.generate({}, function(err, response) {
			res.status(200).send(response.clientToken);
		});
	};
	
	module.exports.run = client_token;
	
	// Initialization function that returns initialized module.exports structure
	var configurator = function(bt) {
		try {
			if(bt)
				config = bt;
			else
				config = require(path.resolve(__dirname, '../../config/braintree.js'));

			gateway = braintree.connect({
				environment: braintree.Environment.Sandbox,
				merchantId: config.merchantId,
				publicKey: config.publicKey,
				privateKey: config.privateKey		
			});
		}
		catch(ex) {};

		return module.exports;
	};
	
	configurator();

	module.exports.config = configurator;
	
}).call(this);