/**
 * Braintree API handler methods to make the Braintree gateway methods available to a client.
 */
;(function() {
	var braintree = require('braintree')
	  , path = require('path')
	  , stringify = require('json-stringify-safe')
	  , clone = require('clone');
	
	var configBt;
	var gateway;

/*
    // Converts URL underscored elements to CamelCase elements
	var toCamelCase = function(s) {
		return s
			.replace(/_(.)/g, function($1) { return $1.toUpperCase(); })
			.replace(/_/g, '')
			.replace(/^(.)/, function($1) { return $1.toLowerCase(); });
	};
*/
	// extract gateway parameters from request object
	var req_params = function(req) {
		
		if(req.method === 'GET') {
			var params = clone(req.query);
		}
		else if(req.method === 'POST') {
			if(typeof req.body === 'string') {
				if(req.headers['content-type'] === 'application/json')
					var params = JSON.parse(req.body);
				else if (req.headers['content-type'] === 'text/plain') {
					var params = req.body.split('\r\n').reduce(function(o, v, i) {
						var a = v.split('=');
						o[a[0]] = a[1];
						return o;
					}, {});
				}
				else
					var params = {};
			}
			else
				var params = clone(req.body);
		}
		else {
			return null;			
		};
					
		return params;	
	};

	// core gateway access routine that can be called by other APIs
	var gateway_fn = function( apireq, apimth, params, callback ) {		
		if(!gateway[apireq] || !gateway[apireq][apimth] || (typeof gateway[apireq][apimth] !== "function")) {;
			return false;
		};

		// do the actual call to the gateway
		gateway[apireq][apimth](params, callback);
		return true;
	};
	
	// HTTP Express handler
	var gateway_request = function( req, res ) {
		
		res.set('Access-Control-Allow-Origin', '*');

		var params = req_params(req);

		// validate the Braintree API request and method
		if(!params || !params.request || !params.method) {
			res.status(404).send();
			return;
		};
				
		var r = params.request;
		var m = params.method;
		
		delete params.request;
		delete params.method;

		function gateway_callback( err, response ) {
			if(err !== null) {
				res.status(500).send(err);
				return;
			};
			res.status(200).send(response);
		};

		if( !gateway_fn(r, m, params, gateway_callback) ) {
			res.status(404).send();
		};
	};	
	
	module.exports.fn = gateway_fn;
	module.exports.hfn = gateway_request;
	module.exports.run = gateway_request;
	
	// Initialization function that returns initialized module.exports structure
	var configurator = function(bt) {
		try {
			if(bt)
				config = bt;
			else
				config = require(path.resolve(__dirname, '../../config/braintree.js'));

			gateway = braintree.connect({
				environment: braintree.Environment.Sandbox,
				merchantId: config.merchantId,
				publicKey: config.publicKey,
				privateKey: config.privateKey		
			});
		}
		catch(ex) {};

		return module.exports;
	};

	module.exports.config = configurator;
	
}).call(this);