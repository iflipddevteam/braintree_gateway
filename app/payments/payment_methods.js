/**
 *  Example Braintree API to get a payment methods
 */
;(function() {
	var braintree = require('braintree')
	  , path = require('path');
	
	var config;
	var gateway;
	
	var payment_methods = function( req, res ) {
		var nonce = req.body.payment_method_nonce;
		
		res.set('Access-Control-Allow-Origin', '*');

		gateway.transaction.sale({
			amount: '10.00',
			paymentMethodNonce: nonce
		}, function(err, response) {
			res.status(200).send(response);
		});
	};
	
	module.exports.run = payment_methods;
	
	// Initialization function that returns initialized module.exports structure
	var configurator = function(bt) {
		try {
			if(bt)
				config = bt;
			else
				config = require(path.resolve(__dirname, '../../config/braintree.js'));
			
			gateway = braintree.connect({
				environment: braintree.Environment.Sandbox,
				merchantId: config.merchantId,
				publicKey: config.publicKey,
				privateKey: config.privateKey		
			});	
		}
		catch(ex){};
		
		return module.exports;
	};
	
	configurator();

	module.exports.config = configurator;
	
}).call(this);