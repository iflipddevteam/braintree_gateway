/**
 * Braintree API handler method to return Backendless Braintree gateway server authorization ping
 */
;(function() {
	var request = require('request')
	, path = require('path')
	, ping_request = require(path.resolve(__dirname, './ping.js')).run
	, stringify = require('json-stringify-safe')
	, clone = require('clone');

	var configBkUser;

	// core cron job API access routine that can be called by other APIs
	var ping_auth_fn = function( ) {		
	};

	// HTTP Express handler
	var ping_auth_request = function( req, res ) {

		// validate the user-token
		if((req.method == 'POST' || req.method == 'PUT') && req.body) {
			var reqBody = JSON.parse(req.body);
			var userToken = reqBody['user-token'];
		}
		if((req.method == 'GET') && req.query) {
			var userToken = req.query['user-token'];
		}
		if (!userToken) {
			res.status(404).send();
			return;			
		};

		// The User user-token is checked against the Backendless instance where user is logged in
		var request_options = {
			method: req.method,
			url: 'https://api.backendless.com/' + configBkUser.appVersion + '/users/isvalidusertoken/' + userToken,
			headers: {
				'application-id': configBkUser.appId,
				'secret-key': configBkUser.restSecretKey,
				'application-type': 'REST'
			}
		};
	
		function request_callback( err, response, body ) {
			if(err !== null) {
				res.status(500).send(err);
				return;
			};

			if(!body || typeof body !== 'string' || !(body === 'true')) {
				res.status(401).send();
				return;
			};
		
			ping_request( req, res );
		};
	
		request.get(request_options, request_callback);
	};

	module.exports.fn = ping_auth_fn;
	module.exports.hfn = ping_auth_request;
	module.exports.run = ping_auth_request;
	
	// Initialization function that returns initialized module.exports structure
	var configurator = function(bku) {
		try {
			if(bku)
				configBkUser = bku;
			else
				configBkUser = require('../config/backendless_user.js');
		}
		catch(ex) {};

		return module.exports;
	};
	
	configurator();

	module.exports.config = configurator;

}).call(this);