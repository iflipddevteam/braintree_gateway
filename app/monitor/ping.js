/**
 * Braintree API handler method to return Backendless Braintree gateway server ping
 */
;(function() {
	var request = require('request')
	  , stringify = require('json-stringify-safe')
	  , clone = require('clone');
	
	// core cron job API access routine that can be called by other APIs
	var ping_fn = function( ) {		
	};
	
	// HTTP Express handler
	var ping_request = function( req, res ) {
			
		if((req.method == 'POST' || req.method == 'PUT') && req.body) {
			res.status(200).send(req.body);
			return;
		};
			
		if (req.method == 'GET') {
			res.status(200).send(req.query);
			return;
		};
			
		res.status(404).send();
	};	
	
	module.exports.fn = ping_fn;
	module.exports.hfn = ping_request;
	module.exports.run = ping_request;
}).call(this);