/**
 * The API to update the status of transactions in the Backendless Data service will go here.
 */
/**
 * Anticipated operation, first version:
 * 1.  Search the User table for set of records that have a PaymentGateway table child record with status
 *     "SUBMITTED FOR SETTLEMENT"(requires automatic Group By operation) and return objects to depth 2. 
 *     If any records are returned, for each User record do steps 2-4:
 * 2.  Extract an array of structures [{customer_id:  customer_id, transaction_ids: [ ]}, … ] from the returned 
 *     records or just walk them as is. For each structure in the array, or equivalent data in the array of records, do steps 3-4:
 * 3.  Search Braintree for records with that "customer_id" and the "transaction_id" values with status "SETTLED".
 * 4.  Update the "status" of PaymentGateway records from returned list of transactions.
 * 5.  Repeat step 1 if the last query returned a full set of records.
 *
 * Backendless search for Step 1 (without paging for now):
 * 
 * curl -i -k \
 *   -H application-id:<Application ID> \
 *   -H secret-key:<REST Secret Key> \
 *   -H application-type:REST \
 *   -X GET https://api.backendless.com/v1/data/users?where=payment_gateway.status%20%3D%201&loadRelations=payment_gateway.payment_method
 */
/**
 * Anticipated operation, second version:
 * 1.  Search the PaymentGateway table for a set of records that have the status "SUBMITTED FOR SETTLEMENT"
 *     and return objects to depth 1.  If any "PaymentGateway" records are returned, do Steps 2-5:
 * 2.  Build an structure of objects (because Step 1 will use paging, this structure will have a limited
 *     number of key-value members):
 *
 *     { payment_method.customer_id: [ transaction.payment_service_transaction_id, ... ], ... }
 *
 * For each member of the structure do Steps 3-5:
 * 3.  Search Braintree for records with the instance's "customer_id" with status "SETTLED".
 * 4.  Prune the argument array of "transaction.payment_service_transaction_id" values to only those 
 *     values in the list of transactions returned by Braintree.
 * 5.  Update the "status" of "PaymentGateway" records from pruned list of transactions.
 * 6.  Repeat Step 1 if the last query returned a full set of records.
 *
 * Backendless search for Step 1 (without paging for now):
 * 
 * curl -i -k \
 * -H application-id:<Application ID> \
 * -H secret-key:<REST Secret Key> \
 * -H application-type:REST \
 * -X GET https://api.backendless.com/v1/data/PaymentGateway?where=status%20%3D%201
 * 
 * Braintree query for Step 3 (proposed server-side, streaming mode)
 * 
 * var stream = gateway.transaction.search(function (search) {
 *   search.customerId().is("the_customer_id");
 *   search.status().is("settled");
 *   search.type().is(braintree.Transaction.Type.Sale);
 *   // avoid Eclipse error checking
 *   //	search.transactionIds().in(["list of ids"]);
 *	 search.transactionIds()['in']( ["list of ids"] );
 * });
 * 
 * https://developers.braintreepayments.com/javascript+node/reference/general/searching/search-results
 * 
 * 
 * Backendless update for Step 5 (proposed)
 * 
 * curl -i -k \
 * -H application-id:0821ED8C-1618-AEA0-FF94-E43038CAF900 \
 * -H secret-key:4108AD86-F385-A274-FF4D-3BBF4AF83400 \
 * -H Content-Type:application/json \
 * -H application-type:REST \
 * -d "{\"status\":2}" \
 * -X PUT https://api.backendless.com/v1/data/PaymentGateway?where=payment_method.customer_id%20<customerID>%20AND%20transaction.payment_service_transaction_id%20IN%20%28<transactionIds>%29
 *
 * See the info on this page for the FOR-loop construction
 * http://www.richardrodger.com/2011/04/21/node-js-how-to-write-a-for-loop-with-callbacks
 */
/**
 * Braintree API handler methods to make the cron job API available to a client.
 */
;(function() {

	var request = require('request')
	  , path = require('path')
	  , braintree = require('braintree')
	  , stringify = require('json-stringify-safe')
	  , clone = require('clone');
	
	var configBkUser;
	var configBt;
	var gateway;
	
	// Put this mapping in this module for now rather than as a separate file
/*	
	var BackendlessTransactionStatus = {
		'authorization_expired': 1,
		'authorizing': 2,
		'authorized': 3,
		'gateway_rejected': 4,
		'failed': 5,
		'processor_declined': 6,
		'settled': 7,
		'settling': 8,
		'settlement_declined': 9,
		'settlement_pending': 10,
		'submitted_for_settlement': 11,
		'voided': 12	
	};
*/
	var BackendlessTransactionStatus = {
		'submitted_for_settlement': 1,
		'settled': 2
	};
	
	// We just put default values for processing loop here for now
	var defaultPageSize = 5
	  , defaultOffset = 0
	  , defaultAll = true;
	
	var defaultUserToken = "";
	var curUserToken;
	
	// Backendless query to update status
	// It is recommended that Backendless user access controls be added to this
	// The cron job should be a user and should login to get a user token, etc.
	// PARTIALLY DEBUGGED (using GET rather than POST)
	// Seems to work
	var queryUpdateStatus = function(params, transactions, callback) {
//console.log('queryUpdateStatus');

		var transactionIds = transactions.reduce( function(x, t) {
			x.push(t.id);
			
			return x;
		}, []);
//console.log(transactionIds);
		
		// may not need to quote 'Braintree' in this query string
		var tIds = '\'' + transactionIds.join('\',\'') + '\'';
		var whereClause = ['payment_method.customer_id=' + params.customerId,
		             	   'payment_method.payment_service=\'Braintree\'',
		             	   'payment_service_transaction_id IN (' + tIds + ')'].join(' AND ');

		var url = 'https://api.backendless.com/' + configBkUser.appVersion + '/data/PaymentGateway' 
					+ '?where=' + encodeURIComponent(whereClause);
			
		var requestOptions = {
			url: url,
			headers: {
				'application-id': configBkUser.appId,
				'secret-key': configBkUser.restSecretKey,
				'application-type': 'REST'
			},
			json: true,
			body: {status: BackendlessTransactionStatus['settled']}
		};
		if(curUserToken)
			requestOptions.headers['user-token'] = curUserToken;
				
		function requestCallback( err, response, body ) {
			if(!response)
				response = {};
			response.cronStep = 'queryUpdateStatus';
			response.cronBody = body;

			if(err !== null) {
				// process error at this update-status level
			};
			
			callback(err, response);
		};
		
		request.get(requestOptions, requestCallback); 
//		request.put(requestOptions, requestCallback); 
				
	};
	
	var queryUpdateStatusStub = function(params, transactions, callback) {
		callback(null, null);
	};
	
	// Braintree query to check status
	// Queries Braintree one customerId at a time
	// PARTIALLY DEBUGGED!
	// Seems to work
	var queryBraintree = function(data, callback) {
//console.log('queryBraintree')
//console.log(data);
		
		var asyncProcessing = function(params, callback) {
//console.log('queryBraintree asyncProcessing')
//console.log(params);
		
			// do the actual call to the gateway
			// This is uses the streaming alternative
			var transactions = [];
			var stream = gateway.transaction.search( function(search) {
				search.customerId().is( params.customerId );
				search.status().is('settled');
//				search.status().is('submitted_for_settlement');
				search.type().is(braintree.Transaction.Type.Sale);
				// avoid Eclipse error checking
//				search.ids().in(["list of ids"]);
				search.ids()['in']( params.transactionIds );
			});
			
			stream.on('error', function( err ) {
				// process errors here
			});
			
			// aggregate transactions that had status "SETTLED"
			stream.on('data', function( transaction ) {
				transactions.push(transaction);
			});
			
			stream.on('end', function() {			
				// bail out in proper asynchronous fashion if no transactions to update
				if(transactions.length <= 0) {
					setImmediate(function () {
						callback(null, null);
					});
				}
				else {
					queryUpdateStatus(params, transactions, function(err, response) {
//					queryUpdateStatusStub(params, transactions, function(err, response) {
						callback(err, response);
					});
				};
			});
		};
		
		
		// FOR-loop like structure
		// This assumes the processingLoop function has an asynchronous operation in it, 
		// otherwise stack overflow could be a risk.
		// Note: This tries to process all records even if an error occurs.
		function forLoop(i) {
			if(i < data.length) {
				asyncProcessing(data[i], function(err, response) {
					// Note lower level errors are not passed forward and processing continues
					forLoop(i+1);
				});
			}
			else
				callback(null, null);
		};
		
		forLoop(0);		
	};
	
	var queryBraintreeStub = function(data, callback) {
		callback(null, null);
	};
	
	// Backendless query to get list of pending transactions.
	// Queries Backendless in fixed-size pages, one page at a time.
	// It is highly recommended that Backendless user access controls be added to this.
	// The cron job should be a user and it may be best to login as that user to get a user token, etc.
	// PARTIALLY DEBUGGED!
	// Seems to work
	var querySubmittedForSettlement = function(pageSize, offset, all, callback) {
//console.log('querySubmittedForSettlement');
//console.log(pageSize);			
//console.log(offset);
//console.log(all);
		
		// This is the internal asynchronous processing
		var asyncProcessing = function(url, callback) {
//console.log('querySubmittedForSettlement asyncProcessing');
//console.log(url);			
			// options for HTTP Request call
			var requestOptions = {
				url: url,
				headers: {
					'application-id': configBkUser.appId,
					'secret-key': configBkUser.restSecretKey,
					'application-type': 'REST'
				}
			};
			if(curUserToken)
				requestOptions.headers['user-token'] = curUserToken;
		
			// call back with next nested level of asynchronous processing			
			function requestCallback( err, response, body ) {
				if(!response)
					response = {};
				response.cronStep = 'querySubmittedForSettlement';
				response.cronBody = body;

				if(err !== null) {
					// process error here
					callback(err, response, null);
					return;
				};

				if(!body || typeof body !== 'string') {
					// process missing body here
					callback(err, response, null);
					return;
				};

				var bodyObj = JSON.parse(body);
				
				// Create a dictionary of elements for the Braintree calls from the returned data by
				// aggregating by the returned transactions by "customerId".
				// Notice camel case used for internal variables
				
				// dataDict is a object:
				// {
				//   customer_id1: {
				//     "customerId": customer_id1,
				//     "transactionIds": [ ... ]
				//   },
				//   ...
				// }
				var dataDict = bodyObj.data.reduce(function(x, paymentGateway) {
					if( paymentGateway.payment_method.payment_service !== 'BrainTree' )
						return x;
					if( !(x[paymentGateway.payment_method.customer_id]) )
						x[paymentGateway.payment_method.customer_id] = 
							{customerId: paymentGateway.payment_method.customer_id, transactionIds: []};
					x[paymentGateway.payment_method.customer_id].transactionIds.push(paymentGateway.payment_service_transaction_id);
					
					return x;
				}, {});
				
				// Reduce this to an array for the Braintree level
				//
				// dataArray is an array:
				// [
				//   {
				//     "customerId": customer_id1,
				//     "transactionIds": [ ... ]
				//   },
				//   ...
				// ]
				//
				var dataArr = Object.keys(dataDict).reduce(function(x, key) {
					x.push(dataDict[key]);
					
					return x;
				}, []);

				// now do the Braintree level and call back to here when done
				// note the 'callback' here is the FOR-loop callback
				// Bail out in proper asynchronous fashion if no pending transactions found
				if(dataArr.length <= 0) {
					setImmediate(function( err, response ) {
						callback(null, null, bodyObj.nextPage);
					});
				}
				else {
					queryBraintree(dataArr, function( err, response ) {
//					queryBraintreeStub(dataArr, function( err, response ) {
						callback(err, response, bodyObj.nextPage);
					});
				};
			};
			
			request.get(requestOptions, requestCallback); 
		};
		
		// Setup to start FOR loop with initial Backendless search URL
		var baseUrl = 'https://api.backendless.com/' + configBkUser.appVersion + '/data/PaymentGateway' 
						+ '?where=' + encodeURIComponent('status='
						+ BackendlessTransactionStatus['submitted_for_settlement'])
						+ '&pageSize=' + pageSize + '&offset=' + offset;

		// FOR-loop like structure
		// This assumes the processingLoop function has an asynchronous operation in it, 
		// otherwise stack overflow could be a risk.
		// Note: This tries to process all records even if an error occurs.
		function forLoop(url) {
			if(url) {
				asyncProcessing(url, function(err, response, nextUrl) {
					// Note lower level errors are not passed forward and processing continues
					forLoop(nextUrl);
				});
			}
			else {
				callback( null, stringify({ nextOffset: null }) );
			};
		};
		
		// choose processing mode (for-loop or single pass) as requested
		// In single pass mode first level errors are returned.
		if(all)
			forLoop(baseUrl);
		else {
			asyncProcessing(baseUrl, function(err, response, nextUrl) {
				if(err)
					callback( err, stringify({ response : response, nextOffset: pageSize} ));
				else if(nextUrl)
					callback( null, stringify({ nextOffset: pageSize + offset }) );
				else
					callback( null, stringify({ nextOffset: null }) );
			});
		};
	};


	// core gateway access routine that can be called by other APIs
	var update_status_fn = function( reqBody, callback ) {

		var curPageSize = defaultPageSize;
		var curOffset = defaultOffset;
		var curAll = defaultAll;
		var curUserToken = defaultUserToken;
		if(reqBody) {
			if( typeof reqBody.pageSize === 'number' && reqBody.pageSize > 0 )
				curPageSize = reqBody.pageSize;
			if( typeof reqBody.offset === 'number' && reqBody.offset >= 0 ) 
				curOffset = reqBody.offset;
			if( typeof reqBody.all === 'boolean' ) 
				curAll = reqBody.all;
			if( reqBody['user-token'] )
				curUserToken = reqBody['user-token'];				
		};
		
		querySubmittedForSettlement(curPageSize, curOffset, curAll, function( err, response ) {
			callback( err, response );
		});
	};
	
	// HTTP Express handler
	var update_status_request = function( req, res ) {
		
		res.set('Access-Control-Allow-Origin', '*');

		function update_status_callback( err, response ) {
			if(err !== null) {
				res.status(500).send(err);
				return;
			};
			
			// set up headers for HTTP response
			res.status(200).send(response);
		};
		
		// determine mode based on parameters supplied in the request
		var reqBody = (req.body ? JSON.parse(req.body) : {});

		update_status_fn( reqBody, update_status_callback );
	};	

	module.exports.fn = update_status_fn;
	module.exports.hfn = update_status_request;
	module.exports.run = update_status_request;
	
	// Initialization function that returns initialized module.exports structure
	var configurator = function(bku, bt) {
		try {
			if(bku)
				configBkUser = bku;
			else
				configBkUser = require(path.resolve(__dirname, '../config/backendless_user.js'));
		}
		catch(ex) {};
		
		try {
			if(bt)
				config = bt;
			else
				config = require(path.resolve(__dirname, '../config/braintree.js'));

			gateway = braintree.connect({
				environment: braintree.Environment.Sandbox,
				merchantId: config.merchantId,
				publicKey: config.publicKey,
				privateKey: config.privateKey		
			});
		}
		catch(ex) {};

		return module.exports;
	};
	
	configurator();

	module.exports.config = configurator;	
	
}).call(this);