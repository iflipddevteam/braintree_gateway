/**
 * Braintree API handler methods to make the cron job API available to a client.
 */
;(function() {
	var request = require('request')
	  , path = require('path')
	  , stringify = require('json-stringify-safe')
	  , clone = require('clone');
	
	var configBkUser;

	var update_status_request = require(path.resolve(__dirname, './update_status2.js')).hfn;

//	Backendless.initApp(configBk.appId, configBk.javascriptSecretKey, configBk.appVersion);

	// core cron job API access routine that can be called by other APIs
	var update_status_auth_fn = function( ) {		
	};
	
	// HTTP Express handler
	var update_status_auth_request = function( req, res ) {
/*
		// validate the Backendless options		
		// Gateway credentials validated against Backendless instance running the gateway
		if(!req.headers['application-id'] || req.headers['application-id'] !== configBk.appId) {
			res.status(404).send();
			return;			
		};
		
		if(!req.headers['secret-key'] || req.headers['secret-key'] !== configBk.restSecretKey) {
			res.status(404).send();
			return;			
		};
*/		
		
		if((req.method == 'POST' || req.method == 'PUT') && req.body) {
			var reqBody = JSON.parse(req.body);
			var userToken = reqBody['user-token'];
		}
		if (!userToken) {
			res.status(404).send();
			return;			
		};

		// The User user-token is checked against the Backendless instance where user is logged in
		var request_options = {
			method: req.method,
//			url: 'https://api.backendless.com/' + configBkUser.appVersion + '/users/isvalidusertoken/' + req.headers['user-token'],
			url: 'https://api.backendless.com/' + configBkUser.appVersion + '/users/isvalidusertoken/' + userToken,
			headers: {
				'application-id': configBkUser.appId,
				'secret-key': configBkUser.restSecretKey,
				'application-type': 'REST'
			}
		};
		
		function request_callback( err, response, body ) {
			if(err !== null) {
				res.status(500).send(err);
				return;
			};

			if(!body || typeof body !== 'string' || !(body === 'true')) {
				res.status(401).send();
				return;
			};
			
			update_status_request(req, res);
		};
		
		request.get(request_options, request_callback);
	};	
	
	module.exports.fn = update_status_auth_fn;
	module.exports.hfn = update_status_auth_request;
	module.exports.run = update_status_auth_request;
	
	// Initialization function that returns initialized module.exports structure
	var configurator = function(bku, bt) {
		try {
			if(bku)
				configBkUser = bku;
			else
				configBkUser = require('../config/backendless_user.js');
		}
		catch(ex) {};
		
		if(bku && bt)
			update_status_request = require('./update_status2.js').config(bku, bt).hfn;

		return module.exports;
	};
	
	configurator();

	module.exports.config = configurator;	

}).call(this);