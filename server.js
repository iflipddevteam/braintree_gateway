/**
 * Module to configure the application
 */
;(function() {
	var fs = require('fs')
	  , path = require('path')
	  , common = require(path.resolve(__dirname, './server/lib/common.js'))
	  , start = require(path.resolve(__dirname, './server/start.js'))
//	  , api = require(path.resolve(__dirname, './server/api.js'));
//	  , api = require(path.resolve(__dirname, './app/api.js'));
	  , api = require(path.resolve(__dirname, './api.js'));
	
	// Load configuration
	try {
		fs.accessSync(path.resolve(__dirname, './config/appserver.js'), fs.R_OK);
		var configAppPrm = require(path.resolve(__dirname, './config/appserver.js'));
	}
	catch (ex) {
		var configAppPrm = {};
		if(process.env.PORT)
			configAppPrm.port = process.env.PORT;
		if(process.env.HOST)
			configAppPrm.host = process.env.HOST;
		if(process.env.PROTOCOL)
			configAppPrm.protocol = process.env.PROTOCOL;
	};
	
	// Set defaults
	if(!configAppPrm.port) { configAppPrm.port = 3000 };
	if(!configAppPrm.host) { configAppPrm.host = 'localhost' };
	if(!configAppPrm.protocol) { configAppPrm.protocol = 'http' };
	
	// API
//	var port = process.env.PORT || '3000';
//	var host = process.env.HOST || 'localhost';
//	var protocol = process.env.PROTOCOL || 'http';

	var optionsApp = {
		server: {
			port: configAppPrm.port,
			host: configAppPrm.host,
			protocol: configAppPrm.protocol
		},
		api: {
			load: api
		}
	};

	// Need to add AWS configuration method
	if( configAppPrm.protocol === 'https') {
		optionsApp.server.tls = {};
		try {
			optionsApp.server.tls.key = fs.readFileSync(path.resolve(__dirname, './cert/key.pem'), 'utf8');
			optionsApp.server.tls.cert = fs.readFileSync(path.resolve(__dirname, './cert/certificate.pem'), 'utf8');
		}
		catch(ex) {
			delete optionsApp.server.tls;
		}
	};

	var app;

	app = start(optionsApp,  function(err) {
		if(!err)
		    console.log('Express app started on port ' + configAppPrm.port);
		else
		    console.log(err, null);
	});

}).call(this);

