#!/bin/bash
#
# Module to run the cron job
# 
# Add to crontab with:
# $ (crontab -l 2>/dev/null; echo "0 3 * * * cron.sh") | crontab -
# 
# Delete from crontab with:
# $ crontab -r -u ec2-user
. $HOME/.bashrc

NODE_PATH=/usr/local/bin
CRON_JOB=/home/ec2-user/braintree_gateway

// Apparently need to do this for something contained in forever or node
export PATH=$NODE_PATH:$PATH
$NODE_PATH/node $CRON_PATH/cron.js