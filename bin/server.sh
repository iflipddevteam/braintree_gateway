#!/bin/bash
#
# Module to run the cron job
# 
# Add to crontab with:
# $ (crontab -l 2>/dev/null; echo "@reboot server.sh") | crontab -
# 
# Delete from crontab with:
# $ sudo crontab -u ec2-user -r
. $HOME/.bashrc

NODE_PATH=/usr/local/bin
SERVER_PATH=/home/ec2-user/braintree_gateway

// Apparently need to do this for something contained in forever or node
export PATH=$NODE_PATH:$PATH
$NODE_PATH/forever start -c $NODE_PATH/node $SERVER_PATH/server.js